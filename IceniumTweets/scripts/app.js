﻿// JavaScript Document
// Wait for PhoneGap to load
document.addEventListener("deviceready", onDeviceReady, false);

// PhoneGap is ready
function onDeviceReady() {
    navigator.splashscreen.hide();
}

function savesettings() {
    localStorage["searchterm"] = $("#searchterm").val();
}

var termToSearch = "#kendoUI";
function readsettings() {
    if (localStorage.searchterm) {
        $("#searchterm").val(localStorage["searchterm"]);
        termToSearch = localStorage["searchterm"];
    } else {
        $("#searchterm").val("#kendoUI");
        termToSearch = "#kendoUI";
    }
}

function showtweets(e) {
    var lastID;

    readsettings();

    // Authenticate to Twitter
    var creds = "6OOpItNEc6RyGgIlNKg:u7i2hnW8w1psfqsn97mvW2XZwsljaPfPrewY5T91DI";
    var encrypted = btoa(creds);
    $.ajax({
        type: "POST",
        url: "https://api.twitter.com/oauth2/token",
        data: { grant_type: "client_credentials" },
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Basic " + encrypted);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        }
    }).done(function (data) {

        var token = data.access_token;

        var tweets = new kendo.data.DataSource({
            serverPaging: true,
            transport: {
                read: {
                    url: "https://api.twitter.com/1.1/search/tweets.json",
                    datatype: "jsonp",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization", "Bearer " + token);
                    }
                },
                parameterMap: function (options) {
                    var parameters = {
                        q: termToSearch,
                        since_id: lastID
                    }
                    return parameters;
                }
            },
            change: function () {
                var item = this.view()[0];
                if (item) {
                    lastID = item.id_str;
                }
            },
            schema: {
                data: "statuses"
            }
        });

        $("#tweetlistview").kendoMobileListView({
            dataSource: tweets,
            pullToRefresh: true,
            appendOnRefresh: true,
            template: kendo.template($("#tweetTemplate").text())
        })


    })


}
